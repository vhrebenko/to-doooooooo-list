var todo = {};
var todoList = {};
var listData = JSON.parse(localStorage.getItem("toDoListData"));
var toDoListData = listData || {};

(function(todo, todoList, toDoListData, $) {      

    // Init generate values
    var defaults = {
            todoTask: "todo-task",
            todoHeader: "task-header",
            todoDate: "task-date",
            todoDescription: "task-description",
            taskId: "task-",
            formId: "todo-form",            
            dataAttribute: "data",
            deleteDiv: "delete-div",
            formEditId: "todo-form-"
        }
    var defaultsToDoList = {
            formId: "todo-list-form",           
            todoListId: "todolist-"
        }    
    var codes = {
            "1" : "pending",
            "2" : "inProgress",
            "3" : "completed"
        };

     // TodoList object init   ---------------------------------------------- 
    todoList.init = function (options) {
        console.log("todoListinit");        
        options = options || {};       
        defaultsToDoList = $.extend({}, defaultsToDoList, options);
        var i = 0;               
        $.each(toDoListData, function (index, params) {            
            generateToDoList(params,i);
            i++;
        }); 
    };

     // Curent TodoList object clear
    todo.clear = function () {
        console.log("clear");        
        var curentToDoList = returnCurentToDoList();        
        toDoListData[curentToDoList.id].todoList = {};       
        localStorage.setItem("toDoListData", JSON.stringify(toDoListData));
        $parent = $("#" + curentToDoList.name);
        var toDoTasks = $parent.find($("." + defaults.todoTask));
        toDoTasks.remove();
    };

    // return curent To Do List id and name
    var returnCurentToDoList = function() {
        var result = {};
        var activeToDoList = $('#navToDoList').find('li.active'); 
        var css_id = activeToDoList.attr("id");
        var activeLink = activeToDoList.find('a'); 
        var todoListName = activeLink.text();
        var todoListId = css_id.replace(defaultsToDoList.todoListId, ""); 
        result["id"] = todoListId;
        result["name"] = todoListName;

        return result; 
    };

    // edit todo task
    todo.edit = function (todoId) {
        console.log("edit");        
        $currentToDo = $("#" + defaults.formEditId + todoId);        
        $currentToDo.show();
    }

    // save result edit todo task
    todo.save = function (todoId) {
        console.log("save"); 
        var curentToDoList = returnCurentToDoList();        
        $currentToDo = $("#" + defaults.taskId + todoId)     
        $currentToDoForm = $("#" + defaults.formEditId + todoId);
        var pTitle = $currentToDo.find("p.task-header");        
        var pDescription = $currentToDo.find("p.task-description");
        var pDate = $currentToDo.find("p.task-date");
        var inputs = $currentToDoForm.find("input");
        var title = inputs[0].value;
        var description = inputs[1].value;
        var date = inputs[2].value;
        toDoListData[curentToDoList.id].todoList[todoId].title = title;
        pTitle.text(title);
        toDoListData[curentToDoList.id].todoList[todoId].description = description;
        pDescription.text(description);
        toDoListData[curentToDoList.id].todoList[todoId].date = date;
        pDate.text(date);
        localStorage.setItem("toDoListData", JSON.stringify(toDoListData));      
               
        $currentToDoForm.hide();
    }

     // Remove todo task object-----------------------------------------------
    todo.remove = function (todoId) {
        console.log("remove");
        var curentToDoList = returnCurentToDoList();           
        object = toDoListData[curentToDoList.id].todoList[todoId];

        // Removing old element        
        removeElementFromToDoList(object, curentToDoList.id);

        // Updating local storage
        delete toDoListData[curentToDoList.id].todoList[todoId];
        localStorage.setItem("toDoListData", JSON.stringify(toDoListData));
    }

    // Remove todo task element from To Do List
    var removeElementFromToDoList = function(params,listId) {
        console.log("removeElementFromToDoList");
        $parent = $("#" + listId);
        $("#" + defaults.taskId + params.id).remove();
    }


    // Add todo task object--------------------------------------------------
    todo.add = function() {
        console.log("todo task add");       
        var toDoList = returnCurentToDoList();          

        var $curentToDoList = $("#" + toDoList.name); 
        var inputs = $curentToDoList.find("#" + defaults.formId + " :input");

        var errorMessage = "Title can not be empty";        

        var title = inputs[0].value;
        var description = inputs[1].value;
        var date = inputs[2].value;

        if (!title) {
            generateDialog(errorMessage);
            return;
        }

        var id = new Date().getTime();

        var tempData = {
            id : id,
            code: "1",
            title: title,
            date: date,
            description: description
        };
        
        toDoListData[toDoList.id].todoList[id] = tempData;        
        
        // Saving element in local storage        
        localStorage.setItem("toDoListData", JSON.stringify(toDoListData));

        // Generate Todo Element        
        generateElementInToDoList(tempData, toDoList.name, toDoList.id);

        // Reset Form
        inputs[0].value = "";
        inputs[1].value = "";
        inputs[2].value = "";
    };

    // Generate todo task element in To Do List
    var generateElementInToDoList = function(params, name, id = null) {
        console.log("generateElementInToDoList");        
        var $curentToDoList = $("#" + name);            
        var parent = $curentToDoList.find($("div[id=" + codes[params.code] + "]"));        

        if (!parent) {
            console.log("error");
            return;
        }

        var wrapper = $("<div />", {
            "class" : defaults.todoTask,
            "id" : defaults.taskId + params.id,
            "data" : params.id
        }).appendTo(parent);

        wrapper.append($("<p>", {
                    "class" : defaults.todoHeader,
                    "text": params.title
                }))
                .append($("<p>", {
                    "class" : defaults.todoDate,
                    "text": params.date
                }))
                .append($("<p>", {
                    "class" : defaults.todoDescription,
                    "text": params.description
                }))
                .append($("<form>", {
                    "class" : "todo-edit",
                    "id"    : "todo-form-" + params.id               
                })
                    .append($("<input>", {
                    "name" : "title",
                    "value" : params.title                                       
                    }))
                    .append($("<input>", {
                    "name" : "description",
                    "value" : params.description                                       
                    }))
                    .append($("<input>", {
                    "name" : "date",
                    "value" : params.date                                       
                    }))
                    .append($("<button>", {
                    "class" : "btn btn-success saveTodo",
                    "text"  : "SAVE",
                    "type"  : "button",
                    "onclick": "todo.save(" + params.id + ")"                    
                    }))
                )
                .append($("<button>", {
                    "class" : "btn btn-danger btn-circle deleteTodo",
                    "onclick": "todo.remove(" + params.id + ")"                    
                })
                   .append($("<i>", {
                    "class" : "glyphicon glyphicon-remove "                                       
                }))) 
                .append($("<button>", {
                    "class" : "btn btn-info btn-circle editTodo",
                    "onclick": "todo.edit(" + params.id + ")"                    
                })
                   .append($("<i>", {
                    "class" : "glyphicon glyphicon-pencil "                                       
                })));  
        $currentToDoForm = $("#todo-form-" + params.id);             
        $currentToDoForm.hide();
        wrapper.draggable({            
            revert: "invalid",
            revertDuration : 200
        });

    }

    // Generate error message ---------------------------------------------
    var generateDialog = function (message) {
        var responseId = "response-dialog",
            title = "Messaage",
            responseDialog = $("#" + responseId),
            buttonOptions;

        if (!responseDialog.length) {
            responseDialog = $("<div />", {
                    title: title,
                    id: responseId
            }).appendTo($("body"));
        }

        responseDialog.html(message);

        buttonOptions = {
            "Ok" : function () {
                responseDialog.dialog("close");
            }
        };

	    responseDialog.dialog({
            autoOpen: true,
            width: 400,
            modal: true,
            closeOnEscape: true,
            buttons: buttonOptions
        });
    };

    // Add To Do List object 
    todoList.add = function() {
        console.log("todoListADD");
        var $activeToDoList = $("#navToDoList").find("li[class=active]");        
        var inputs = $("#" + defaultsToDoList.formId + " :input");
        var errorMessage = "Name To DO List can not be empty";        

        var todoListName = inputs[0].value;        

        if (!todoListName) {
            generateDialog(errorMessage);
            return;
        }

        var id = new Date().getTime();

        var tempData = {
            id : id,           
            name: todoListName,
            todoList: {}
        };

        // Saving element in local storage
        toDoListData[id] = tempData;
        localStorage.setItem("toDoListData", JSON.stringify(toDoListData));

        var count = Object.keys(toDoListData).length;

        // Generate Todo Element
        if(count > 1) {
            generateToDoList(tempData);
        } else {
            generateToDoList(tempData,0);
        }
        
        

        // Reset Form
        inputs[0].value = "";       
    }

    // Add To Do List element----------------------------------------------
    var generateToDoList = function(params,iter = 1){    
        console.log("generateToDoLis");        

        var todoList = params.todoList;
        var $parent =  $("#navToDoList");
        var $parentContent = $("#tabContentToDoList");         
        var $copyHomeContent = $("#homeContent").clone();
        $copyHomeContent.attr("id", "todoList-content-" + params.name);
        var listId = params.id;
        var listName = params.name;
        
        
        var $toDoList = $("<li/>", {            
            "id" : defaultsToDoList.todoListId + listId,
            "data" : listId,
            "class" : "active"
        });

        var $toDoListContent = $("<div/>", {            
            "id" : params.name,
            "class" : "tab-pane fade in active todo-list-content"            
        }); 
        

        $toDoList.append($("<a>", {            
                    "data-toggle" : "tab",
                    "href" : "#" + params.name,
                    "text": params.name
                })); 

        $parent.append($toDoList);       
        $parentContent.append($toDoListContent);   

        $toDoListContent.append($("<button>", {                    
                           "class" : "btn btn-danger deleteTodoList",
                           "onclick": "todoList.remove(" + listId + ")",
                           "text": "DELETE TO DO LIST" 
                        }))
                        .append($copyHomeContent); 

        $curentToDoListContent = $("#todoList-content-" + params.name);  

         // Adding drop function to each category of task
        $.each(codes, function (index, value) {
            $curentElement = $curentToDoListContent.find($("div[id=" + value + "]"));            
            $curentElement.droppable({
                drop: function (event, ui) {
                        var element = ui.helper;                        
                        var css_id = element.attr("id");                        
                        var id = css_id.replace(defaults.taskId, "");                        
                        var object = toDoListData[listId].todoList[id];                        

                            // Removing old element                           
                            removeElementFromToDoList(object,listId);

                            // Changing object code
                            object.code = index;

                            // Generating new element                           
                            generateElementInToDoList(object,listName,listId);

                            // Updating Local Storage
                            toDoListData[listId].todoList[id] = object;
                            localStorage.setItem("toDoListData", JSON.stringify(toDoListData));                           
                    }
            });
        });             
                        
        var $inputDatepicker = $copyHomeContent.find('input.datepicker'); 
        $inputDatepicker.removeClass("hasDatepicker");
        $inputDatepicker.attr("id","");                     
        $inputDatepicker.datepicker();
        $inputDatepicker.datepicker("option", "dateFormat", "dd/mm/yy");  

        $.each(todoList, function (index, params) {            
            generateElementInToDoList(params,listName,listId);            
        });  

        if(iter > 0) {
            var $curActiveLink = $parent.find($("li[id=" + defaultsToDoList.todoListId + listId + "]"));            
            $curActiveLink.removeClass("active");
            var $curActiveContent = $parentContent.find($("div[id=" + params.name + "]"));            
            $curActiveContent.removeClass("in active");
        }

    };
   
    // Remove To Do List object -------------------------------------------
    todoList.remove = function (todoListId) {
        console.log("removeToDoList");        
        object = toDoListData[todoListId];

        removeToDoList(object);

        delete toDoListData[todoListId];
        localStorage.setItem("toDoListData", JSON.stringify(toDoListData));
    }

    // Remove To Do List element ------------------------------------------
    var removeToDoList = function (params) {
        console.log("removeToDoList");        
        $("#" + defaultsToDoList.todoListId + params.id).remove();
        $("#" + params.name).remove();
        $("#navToDoList li:first-child").addClass("active");
        var $curentLink = $("#navToDoList li:first-child").find($("a"));
        var curentToDoListId = $curentLink.text();        
        var $curentContent = $("#" + curentToDoListId);
        $curentContent.addClass("in active");        
    };

})(todo, todoList, toDoListData, jQuery);